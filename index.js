/**
 * Convert an IPv4 address in the format of null-terminated C string into a 32-bit integer
 *
 * @param {string} ipv4 A string with spaces between two digits is not a valid input
 * @return {number} 32-bit
 */
export default (ipv4) => {
  // invalid input
  if (!/^\b(( *\d{1,3} *(?!\d))(\.|$)){4}\b/.test(ipv4)) {
    throw new Error('Error! A string with spaces between two digits is not a valid input');
  }
  return ipv4.split('.').reduce((preInt, currentInt, index) =>
    preInt + currentInt * [16777216, 65536, 256, 1][index]
  , 0);
}
