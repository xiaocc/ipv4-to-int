import ipv4ToInt from './index.js';

test('convent "172.168.5.1" to equal 2896692481', () => {
  expect(ipv4ToInt('172.168.5.1')).toBe(2896692481);
});

test('convert "172 . 168.5.1" to equal 2896692481', () => {
  expect(ipv4ToInt('172 . 168.5.1')).toBe(2896692481);
});

test('throws on "1 72.168.5.1"', () => {
  expect(() => {
    ipv4ToInt('1 72.168.5.1');
  }).toThrow();
});
