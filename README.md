# ipv4-to-int
Convert an IPv4 address in the format of null-terminated C string into a 32-bit integer

## Usage

``` js
import ipv4ToInt from 'ip4-to-int/index.js';
ipv4ToInt('172.168.5.1');
```
